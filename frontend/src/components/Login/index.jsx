import axios from "axios";
import React from "react";
import FacebookLogin from "react-facebook-login";
import GoogleLogin from "react-google-login";
import MicrosoftLogin from "react-microsoft-login";

export const AZURE_CONFIG = {
  TENANT_ID: "06f1b89f-07e8-464f-b408-ec1b45703f31",
  CLIENT_ID: "06a669c6-6e3e-4f01-a624-7f080e92d7e6",
  REDIRECT_URI: "http://localhost:3000",
  SCOPE: "https://graph.microsoft.com/User.Read",
  STATE: "0123456789abcdef",
  URL_GET_ACCESS:
    "https://login.microsoftonline.com/06f1b89f-07e8-464f-b408-ec1b45703f31",
};

const Login = (props) => {
  const responseFacebook = (response) => {
    console.log(response);
    let data = {
      accessToken: response.accessToken,
      rememberMe: false,
    };
    axios.post("http://localhost:8091/api/authenticate/facebook", data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
  };

  const responseGoogle = (response) => {
    console.log(response);
    let data = {
      idToken: response.tokenId,
      rememberMe: false,
    };
    console.log(data);
    axios.post("http://localhost:8091/api/authenticate/google", data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
  };

  const [msalInstance, onMsalInstanceChange] = React.useState();
  const authHandler = (err, data, msal) => {
    console.log(err, data, msal);
    if (!err && data) {
      axios.post(
        "http://localhost:8091/api/authenticate/microsoft/flow2",
        data,
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        }
      );
      onMsalInstanceChange(msal);
    }
  };

  const logoutHandler = () => {
    msalInstance.logout();
  };

  React.useEffect(() => {
    if (
      window.location.href.indexOf("code") > -1 &&
      window.location.href.indexOf("state") > -1
    ) {
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const authorizationCode = urlParams.get("code");
      const state = urlParams.get("state");
      if (state === AZURE_CONFIG.STATE && authorizationCode !== "") {
        loginSSOMicrosoftWithAuthCode(authorizationCode);
      }
    }
  }, []);

  const loginSSOMicrosoftWithAuthCode = (authorizationCode) => {
    const data = {
      authorizationCode: authorizationCode,
    };
    axios.post("http://localhost:8091/api/authenticate/microsoft/flow1", data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
  };

  function getAuthorizationCode() {
    window.location.href =
      "https://login.microsoftonline.com/" +
      AZURE_CONFIG.TENANT_ID +
      "/oauth2/v2.0/authorize?\n" +
      "client_id=" +
      AZURE_CONFIG.CLIENT_ID +
      "&response_type=code\n" +
      "&redirect_uri=" +
      AZURE_CONFIG.REDIRECT_URI +
      "&response_mode=query\n" +
      "&scope=https://graph.microsoft.com/User.Read\n" +
      "&state=" +
      AZURE_CONFIG.STATE;
  }

  return (
    <div className="">
      <FacebookLogin
        appId="821879938772852"
        // autoLoad={true}
        fields="name,email,picture"
        callback={responseFacebook}
        icon="fa-facebook"
      />
      <br />
      <br />

      <GoogleLogin
        clientId="389916474771-adesvsiho4iqqf4ibltu7ldj7g1ocgpb.apps.googleusercontent.com"
        buttonText="LOGIN WITH GOOGLE"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />
      <br />
      <br />
      {msalInstance ? (
        <button onClick={logoutHandler}>Logout microsoft</button>
      ) : (
        <MicrosoftLogin
          clientId="2d52c557-4178-4903-b636-43630367c878"
          authCallback={authHandler}
          tenantUrl="https://login.microsoftonline.com/af3bd586-cbcd-4935-afbb-1cef18d79c7a"
        />
      )}
      <button onClick={getAuthorizationCode}>Login flow 1 microsoft</button>
      <br />
      <br />
    </div>
  );
};

export default Login;
