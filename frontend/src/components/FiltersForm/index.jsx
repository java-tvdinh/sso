// Tạo nhanh gõ rsfp
import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';

FiltersForm.propTypes = {
    onSubmit: PropTypes.func,
    defaultValues: PropTypes.object,    
};

FiltersForm.defaultProps = {
    onSubmit: null,
    defaultValues: {
        keyword: "hellooooo",
    },
}

function FiltersForm(props) {
    const {onSubmit, defaultValues} = props;
    const [searchKey, setSearchKey] = useState('');
    React.useEffect(() => {
        setSearchKey(defaultValues.keyword || "");
        // eslint-disable-next-line react-hooks/exhaustive-deps
      }, [defaultValues]);
    /**
     * useRef: Giúp tạo object và ko thay đổi giữa các lần render
     * Từ lúc component được sinh ra(Lúc tạo app) --> mất đi
     * Lần đầu tiên là null
     */
    const typingTimeoutRef = useRef(null);

    function handleSearchKeyChange(e) {
        setSearchKey(e.target.value);

        if (!onSubmit) return;

        //Kiểm tra lần trước có timeout chưa
        if (typingTimeoutRef.current) {
            clearTimeout(typingTimeoutRef.current);
        }
        //set timeout 300ms ms call onSubmit
        typingTimeoutRef.current = setTimeout(() => {
            // const formValues = {
            //     searchKey, //set như vầy nó sẽ lấy giá trị cũ thay vì lấy giá trị setSearchKey(e.target.value);
            // };
            const formValues = {
                    searchKey: e.target.value,
             };
            onSubmit(formValues);
        }, 300);        
    }

    function handleChangeSort(e) {
        //xem sự khác nhau giữa default value và value
        setSearchKey("jdlklsfad");
    }

    return (
        <form onSubmit={onSubmit}>
            <input
            // defaultValue={defaultValues?.keyword} chỉ được sử dụng lần đầu tiên khi value không xác định,
            //defaultValuevà value không bao giờ được sử dụng cùng nhau trong một phần tử biểu mẫu
            // Khi setSearchKey(newValue) ==> bị thay đổi
            type="text"
            value={searchKey}
            onChange={handleSearchKeyChange}
            />

            <input
            // Khi setSearchKey(newValue) ==> Không bị thay đổi
            type="text"
            defaultValue={searchKey}
            onChange={handleSearchKeyChange}
            />

            <select
              className="form-control form-control-sm"
              defaultValue="1"
              onChange={handleChangeSort}
            >
              <option value="0" disabled>
                --- Sắp xếp theo ---
              </option>
              <option value="1">Bài viết mới nhất</option>
              <option value="2">Bài viết cũ nhất</option>
              <option value="3">Bài viết từ a-z</option>
            </select>
        </form>
    );
}

export default FiltersForm;