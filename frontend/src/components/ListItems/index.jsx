// Tạo nhanh gõ rsfp
// protype: gõ pt --> pta tab thì ra PropTypes.array
import React from 'react';
import PropTypes from 'prop-types';

ListItems.propTypes = {
    items: PropTypes.array,
    onItemClick: PropTypes.func,
};

/**
 * Có ý nghĩa khi cha ko truyền giá trị
 * Nó sẽ lấy giá trị mặc định ở đây
 */
ListItems.defaultProps = {
    items: [],
    onItemClick: null,
}

function ListItems(props) {
    const {items, onItemClick} = props;
    function handleClick(item) {
        if (onItemClick) {
            onItemClick(item);
        }
    }

    return (
        <>
            {items.map((item, index) => (
               <div key={index} onClick={()=> handleClick(item)}>{item.name}</div>
            ))}
        </>
    );
}

export default ListItems;