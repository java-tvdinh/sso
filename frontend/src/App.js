import logo from './logo.svg';
import './App.css';
import FiltersForm from './components/FiltersForm';
import ListItems from './components/ListItems';
import { useEffect, useState } from 'react';
import Pagination from './components/Pagination';
import Login from './components/Login';
//import { useHistory } from 'react-router';

function App() {
  //const history = useHistory();
  const [lst, setLst] = useState([
    { id: 1, name: "Test 1" },
    { id: 2, name: "Test 2" },
    { id: 3, name: "Test 3" },
  ]);
  const [pagination, setPagination] = useState({
    _page: 1,
    _limit: 5,
    _totalRows: 11,
  });
  const [filters, setFilters] = useState({
    _limit: 10,
    _page: 1,
    keyword: '',
  });

  useEffect(() => {
    //call api khi filter thay đổi
    console.log('Search By: ', filters);
    // setLst();
    // setPagination();
  }, [filters]);

  function handleFilterChange(newFilters) {
    console.log('New filters: ', newFilters);
    setFilters({
      ...filters,
      _page: 1, //rest page 1 khi search
      keyword: newFilters.searchKey,
    })
  }

  function handleItemClick(item) {
    console.log("Item click: ", item);
  }

  function handlePageChange(newPage) {
    console.log('New Page:', newPage);
  }

  useEffect(() => {
    if (
      window.location.href.indexOf("id_token") > -1 &&
      window.location.href.indexOf("state") > -1
    ) {
      // get param from url
      const queryString = window.location.href;
      const queryParams = queryString.split("#")[1];
      const urlParams = new URLSearchParams(queryParams);
      const id_token = urlParams.get("id_token");
      const state = urlParams.get("state");
      const expires = urlParams.get("expires_in");
      const token_type = urlParams.get("token_type");
      const refresh_token = urlParams.get("refresh_token");
      const access_token = urlParams.get("access_token");
      const session_id = urlParams.get("sessionId");

      const accessToken = { id_token, expires, token_type, session_id, refresh_token };

      if (state === process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_STATE && id_token !== "") {
        // loginSmartOfficeSSO(id_token, state, expires, token_type, refresh_token, session_id)
        //   .then(res => {
        //     fetchUserInfo().then(user => {
        //       //history.push(`/`);
        //     });
        //   })
        //   .catch(err => {
        //     console.log(err);
        //   });
      }
    } else {
    }
  }, []);

  const token = localStorage.getItem('ACCESS_TOKEN');
  if (!token) {
    // if (window.location.pathname != loginUrl) {
    //   window.location.href = loginSsoUrl;
    // } else {
    //   const queryString = window.location.href;
    //   const queryParams = queryString.split("#")[1];
    //   const urlParams = new URLSearchParams(queryParams);
    //   const id_token = urlParams.get("id_token");
    //   if (id_token == null) {
    //     window.location.href = loginSsoUrl;
    //   }
    // }
  }

  // if (window.location.href.indexOf(logoutUrl) > -1) {
  //   window.location.href = loginUrl;
  // }

  return (
    <div className="App">
      <h1>Hello world!!!</h1>
      <a>Login SSO</a>
      <Login />
      <FiltersForm onSubmit={handleFilterChange} />
      <ListItems items={lst} onItemClick={handleItemClick} />
      <Pagination pagination={pagination} onPageChange={handlePageChange} />
    </div>
  );
}

export default App;

export const getUrlLoginSso = () => {
  const clientIdEncodeUri = encodeURI(
    "" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_CLIENT_ID,
  );
  const redirectEncodeUri = encodeURI(
    "" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_REDIRECT_URI,
  );
  const responseTypeEncodeUri = encodeURI(
    "" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_RESPONSE_TYPE,
  );
  const scopeEncodeUri = encodeURI("" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_SCOPE);
  const nonceEncodeUri = encodeURI("" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_NONCE);
  const stateEncodeUri = encodeURI("" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_STATE);
  const tenantCodeEncodeUri = encodeURI(
    "" + process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_TENANT_CODE,
  );

  return (
    process.env.REACT_APP_AUTHORIZATION_SSO_SMART_OFFICE_BASE_URL +
    "?response_type=" +
    responseTypeEncodeUri +
    "&scope=" +
    scopeEncodeUri +
    "&nonce=" +
    nonceEncodeUri +
    "&state=" +
    stateEncodeUri +
    "&client_id=" +
    clientIdEncodeUri +
    "&redirect_uri=" +
    redirectEncodeUri +
    "&tenant_code=" +
    tenantCodeEncodeUri
  );
};
