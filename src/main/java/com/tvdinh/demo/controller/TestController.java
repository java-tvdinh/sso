package com.tvdinh.demo.controller;

import com.tvdinh.demo.domain.request.GoogleLogin;
import com.tvdinh.demo.domain.request.LoginRequest;
import com.tvdinh.demo.domain.request.LoginSSOKeyCloak;
import com.tvdinh.demo.domain.request.MicrosoftLoginFlow1;
import com.tvdinh.demo.domain.request.MicrosoftLoginFlow2;
import com.tvdinh.demo.domain.request.SocialLogin;
import com.tvdinh.demo.domain.response.AuthToken;
import com.tvdinh.demo.service.DemoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
public class TestController {

    private final DemoService demoService;

    public TestController(DemoService demoService) {
        this.demoService = demoService;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthToken> login(@RequestBody LoginRequest request) {
        return ResponseEntity.ok(demoService.login(request));
    }

    @PostMapping("/authenticate/keycloak")
    public ResponseEntity<AuthToken> loginSSOByKeyCloak(@RequestBody LoginSSOKeyCloak request) {
        return ResponseEntity.ok(demoService.loginSSOByKeyCloak(request));
    }

    @PostMapping("/authenticate/facebook")
    public ResponseEntity<AuthToken> loginSSOByFacebook(@RequestBody SocialLogin request) {
        return ResponseEntity.ok(demoService.loginSSOByFacebook(request));
    }

    @PostMapping("/authenticate/google")
    public ResponseEntity<AuthToken> loginSSOByGoogle(@RequestBody GoogleLogin request) {
        return ResponseEntity.ok(demoService.loginSSOByGoogle(request));
    }

    @PostMapping("/authenticate/microsoft/flow2")
    public ResponseEntity<AuthToken> loginSSOByMicrosoftFlow2(@RequestBody MicrosoftLoginFlow2 request) {
        return ResponseEntity.ok(demoService.loginSSOByMicrosoftFlow2(request));
    }

    @PostMapping("/authenticate/microsoft/flow1")
    public ResponseEntity<AuthToken> loginSSOByMicrosoftFlow1(@RequestBody MicrosoftLoginFlow1 request) {
        return ResponseEntity.ok(demoService.loginSSOByMicrosoftFlow1(request));
    }

    @GetMapping("/authenticate/microsoft/gen-logout-page")
    public RedirectView getMicrosoftLogoutSsoUrl(@RequestParam(value = "id_token") String idToken, HttpServletResponse response) {
        return demoService.getMicrosoftLogoutSsoUrl(idToken, response);
    }

    @GetMapping("/authenticate/microsoft/gen-login-page")
    public RedirectView getMicrosoftLoginSsoUrl(HttpServletResponse response) {
        return demoService.getMicrosoftLoginSsoUrl(response);
    }
}
