package com.tvdinh.demo.controller;

import com.tvdinh.demo.domain.ClientDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api")
public class ClientResource {

    /**
     * Giả sử chưa có client id và scret thì ta sẽ tự sinh ra rồi cho vào file .yml
     *
     * Ex: có 1 ứng dụng khác tên là B muốn sử dụng 1 api /xx bên A
     * b1. Bên A sinh clientId, clientSecret
     * b2. Bên B sẽ lấy tokent(clientId, clientSecret) từ A bằng cách sử dụng token từ bên B call api /authen/b ở bên A
     * b3. Bên A lấy được token bên B sẽ parser để lấy thông tin từ token(chứa user info) của B rồi map với user bên A
     * b3. Sau khi map được user bên A -> sinh token chứa (clientId, clientSecret) để bên B call api
     * b4. Bên B call api /xx gắn token chứa (clientId, clientSecret)
     */
    @GetMapping("/client/generate/app")
    public ResponseEntity<ClientDTO> generateClientIdAndSecret() {
        String clientId = UUID.randomUUID().toString();
        String clientSecret = RandomStringUtils.randomAlphanumeric(100);
        return ResponseEntity.ok(ClientDTO.builder().clientId(clientId).clientSecret(clientSecret).build());
    }

}
