package com.tvdinh.demo.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {

    @JsonProperty("error_msg")
    private String errorMsg;

    @JsonProperty("error_code")
    private String errorCode;

}
