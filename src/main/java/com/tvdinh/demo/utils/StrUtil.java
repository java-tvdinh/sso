package com.tvdinh.demo.utils;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class StrUtil {

    public static String removeAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isBlank(Object str) {
        return str == null || ((String) str).trim().length() == 0;
    }
}
