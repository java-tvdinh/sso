package com.tvdinh.demo.domain;

import lombok.Data;

@Data
public class User {
    private Long id;
    private String login;
    private String password;
    private String name;
    private String email;
    private String avatar;
}
