package com.tvdinh.demo.domain.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class SocialLogin {
    @NotNull
    @Size(min = 1, max = 255)
    private String accessToken;

    private Boolean rememberMe;
}
