package com.tvdinh.demo.domain.request;

import lombok.Data;

@Data
public class MicrosoftLoginFlow2 {
    private String accessToken;
}
