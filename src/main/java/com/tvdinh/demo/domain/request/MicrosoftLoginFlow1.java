package com.tvdinh.demo.domain.request;

import lombok.Data;

@Data
public class MicrosoftLoginFlow1 {
    private String authorizationCode;
}
