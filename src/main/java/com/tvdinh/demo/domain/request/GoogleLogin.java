package com.tvdinh.demo.domain.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GoogleLogin {
    @NotNull
    private String idToken;

    private Boolean rememberMe;
}
