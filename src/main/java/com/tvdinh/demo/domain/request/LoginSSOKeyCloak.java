package com.tvdinh.demo.domain.request;

import lombok.Data;

@Data
public class LoginSSOKeyCloak {
    private String code;
    private String grantType;
    private String redirectUri;
}
