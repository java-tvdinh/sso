package com.tvdinh.demo.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.restfb.Facebook;
import lombok.Data;

@Data
public class UserDTO {

    private Long id;

    @Facebook("id")
    private String login;

    @Facebook("name")
    private String name;

    @Facebook("email")
    private String email;

    @Facebook("first_name")
    private String firstName;

    @Facebook("last_name")
    private String lastName;

    private String avatar;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    private String socialId;

    private String socialType;
}
