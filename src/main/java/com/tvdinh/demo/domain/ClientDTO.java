package com.tvdinh.demo.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientDTO {
    private String clientId;
    private String clientSecret;
}
