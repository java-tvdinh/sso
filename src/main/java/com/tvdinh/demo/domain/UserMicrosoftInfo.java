package com.tvdinh.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserMicrosoftInfo {
    private String id;
    private String mail;
    private String userPrincipalName;
    private String displayName;
}
