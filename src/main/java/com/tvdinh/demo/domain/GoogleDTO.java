package com.tvdinh.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleDTO {
    private String iss;
    private String azp;
    private String aud;
    private String sub;
    private String hd;
    private String email;
    private String email_verified;
    private String name;
    private String picture;
    private String given_name;
    private String family_name;
    private String iat;
    private String exp;
    private String alg;
    private String kid;
    private String typ;

    private String locale;

    private String at_hash; // IOS
    private String nonce; //IOS
}
