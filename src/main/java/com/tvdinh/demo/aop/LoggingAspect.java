package com.tvdinh.demo.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class LoggingAspect {
    @Pointcut("within(com.tvdinh.demo.controller..*) || within(com.tvdinh.demo.service.impl..*)")
    public void logBeforeFunctionPointcut() {

    }

    @Before("logBeforeFunctionPointcut()")
    public void logBeforeFunctionAdvice(JoinPoint joinPoint) {
        log.info("Class {}. Function {}() with argument[s] = {}", joinPoint.getTarget().getClass().getSimpleName(),
                joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
    }
}
