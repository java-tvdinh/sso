package com.tvdinh.demo.service;

import com.tvdinh.demo.domain.request.GoogleLogin;
import com.tvdinh.demo.domain.request.LoginRequest;
import com.tvdinh.demo.domain.request.LoginSSOKeyCloak;
import com.tvdinh.demo.domain.request.MicrosoftLoginFlow1;
import com.tvdinh.demo.domain.request.MicrosoftLoginFlow2;
import com.tvdinh.demo.domain.request.SocialLogin;
import com.tvdinh.demo.domain.response.AuthToken;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;

public interface DemoService {

    AuthToken login(LoginRequest request);

    AuthToken loginSSOByKeyCloak(LoginSSOKeyCloak request);

    AuthToken loginSSOByFacebook(SocialLogin request);

    AuthToken loginSSOByGoogle(GoogleLogin request);

    AuthToken loginSSOByMicrosoftFlow2(MicrosoftLoginFlow2 request);

    AuthToken loginSSOByMicrosoftFlow1(MicrosoftLoginFlow1 request);

    RedirectView getMicrosoftLogoutSsoUrl(String idToken, HttpServletResponse response);

    RedirectView getMicrosoftLoginSsoUrl(HttpServletResponse response);
}
