package com.tvdinh.demo.service;

import com.tvdinh.demo.domain.User;
import com.tvdinh.demo.domain.UserDTO;

import java.util.Optional;

public interface UserService {

    Optional<User> findByEmail(String email);

    Optional<User> findByLogin(String login);

    User createUser(UserDTO userDTO);

    User authenticationSocialUser(UserDTO userDTO);

}
