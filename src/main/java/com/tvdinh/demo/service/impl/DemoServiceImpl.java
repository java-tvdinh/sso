package com.tvdinh.demo.service.impl;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.json.JsonObject;
import com.tvdinh.demo.config.AuthenticationProperties;
import com.tvdinh.demo.config.TokenProvider;
import com.tvdinh.demo.domain.GoogleDTO;
import com.tvdinh.demo.domain.User;
import com.tvdinh.demo.domain.UserDTO;
import com.tvdinh.demo.domain.UserInfo;
import com.tvdinh.demo.domain.UserMicrosoftInfo;
import com.tvdinh.demo.domain.request.GoogleLogin;
import com.tvdinh.demo.domain.request.LoginRequest;
import com.tvdinh.demo.domain.request.LoginSSOKeyCloak;
import com.tvdinh.demo.domain.request.MicrosoftLoginFlow1;
import com.tvdinh.demo.domain.request.MicrosoftLoginFlow2;
import com.tvdinh.demo.domain.request.SocialLogin;
import com.tvdinh.demo.domain.response.AuthToken;
import com.tvdinh.demo.service.DemoService;
import com.tvdinh.demo.service.UserService;
import com.tvdinh.demo.utils.RestTemplateHelper;
import com.tvdinh.demo.utils.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class DemoServiceImpl implements DemoService {

    private final RestTemplateHelper restTemplateHelper;
    private final UserService userService;
    private final TokenProvider tokenProvider;
    private final AuthenticationManager authenticateManager;
    private final AuthenticationProperties authenticationProperties;

    public DemoServiceImpl(RestTemplateHelper restTemplateHelper,
                           UserService userService,
                           TokenProvider tokenProvider,
                           AuthenticationManager authenticateManager,
                           AuthenticationProperties authenticationProperties) {
        this.restTemplateHelper = restTemplateHelper;
        this.userService = userService;
        this.tokenProvider = tokenProvider;
        this.authenticateManager = authenticateManager;
        this.authenticationProperties = authenticationProperties;
    }

    @Override
    public AuthToken login(LoginRequest request) {
        log.info("User {} start login", request.getUsername());
        User user = new User();
        user.setId(111L);
        user.setLogin(request.getUsername());
        user.setPassword(request.getPassword());

        Authentication authentication = new UsernamePasswordAuthenticationToken(request.getUsername().toLowerCase(),
                request.getPassword(), new ArrayList<>());

        //Check ldap or database using CustomAuthenticationProvider
        authentication = authenticateManager.authenticate(authentication);

        String accessToken = tokenProvider.createToken(authentication, user.getId());
        long expiresIn = authenticationProperties.getAccessTokenExpiresIn().toSeconds();
        String refreshToken = null;
        if (request.isRememberMe()) {
            refreshToken = tokenProvider.createRefreshToken(user.getId());
        }
        log.info("User {} login success", request.getUsername());

        return AuthToken.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .tokenType("Bearer")
                .expiresIn(expiresIn)
                .build();
    }

    @Override
    public AuthToken loginSSOByKeyCloak(LoginSSOKeyCloak request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        String clientId = "app-client";
        String clientSecret = "BQdLdpq2Xp3SlJtgnJGgeFzAmKaypc7Q";

        String tokenHeader = Base64.getEncoder().encodeToString(String.format("%s:%s", clientId, clientSecret).getBytes(StandardCharsets.UTF_8));
        headers.setBasicAuth(tokenHeader);

        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("grant_type", "authorization_code");
        formData.add("code", request.getCode());
        formData.add("redirect_uri", "http://172.25.176.1:8080/");

        // Call access token
        String apiSso = "http://192.168.56.3:8080/auth/realms/test/protocol/openid-connect/token";
        AuthToken authTokenSSO = restTemplateHelper.executeWithPredefinedHeader(apiSso, HttpMethod.POST, headers, formData, AuthToken.class);
        if (authTokenSSO == null) {
            throw new BadCredentialsException("Bad credentials");
        }

        // Call user info
        String apiUserInfo = "http://192.168.56.3:8080/auth/realms/test/protocol/openid-connect/userinfo";
        headers.clear();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(authTokenSSO.getAccessToken());
        UserInfo userInfo = restTemplateHelper.executeWithPredefinedHeader(apiUserInfo, HttpMethod.GET, headers, UserInfo.class);

        return AuthToken.builder()
                .accessToken(authTokenSSO.getAccessToken())
                .refreshToken(authTokenSSO.getRefreshToken())
                .expiresIn(authTokenSSO.getExpiresIn())
                .build();
    }

    @Override
    public AuthToken loginSSOByFacebook(SocialLogin request) {
        FacebookClient facebookClient = new DefaultFacebookClient(request.getAccessToken(), Version.LATEST);
        UserDTO user = facebookClient.fetchObject("me", UserDTO.class, Parameter.with("fields", "id,name,email,first_name,last_name"));

        Optional<User> optionalUser = userService.findByLogin(user.getLogin());
        //Test
        User test = new User();
        test.setId(2222L);
        test.setLogin(user.getLogin());
        optionalUser = Optional.of(test);

        if (optionalUser.isEmpty()) {
            optionalUser = userService.findByEmail(user.getEmail());
            if (optionalUser.isEmpty()) {
                // Case: chưa tồn tại trong hệ thống
                JsonObject jsonObject = facebookClient.fetchObject("/" + user.getLogin() + "/picture", JsonObject.class,
                        Parameter.with("type", "large"), Parameter.with("redirect", "false"));
                user.setAvatar(jsonObject.get("data").asObject().get("url").toString());
                user.setPassword(RandomStringUtils.randomAlphabetic(10));
                //optionalUser = Optional.of(userService.createUser(user));
                //Test
            }
        }
        Authentication authentication = new UsernamePasswordAuthenticationToken(optionalUser.get().getLogin(), null,
                new ArrayList<>());
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String accessToken = tokenProvider.createToken(authentication, optionalUser.get().getId());
        long expiresIn = authenticationProperties.getAccessTokenExpiresIn().toSeconds();
        String refreshToken = null;
        if (Boolean.TRUE.equals(request.getRememberMe())) {
            refreshToken = tokenProvider.createRefreshToken(optionalUser.get().getId());
        }
        return AuthToken.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .tokenType("Bearer")
                .expiresIn(expiresIn)
                .build();
    }

    @Override
    public AuthToken loginSSOByGoogle(GoogleLogin request) {
        String link = "https://oauth2.googleapis.com/tokeninfo?id_token=" + request.getIdToken();
        GoogleDTO googleDTO = restTemplateHelper.execute(
                link, HttpMethod.GET, null, GoogleDTO.class);
        if (googleDTO != null) {
            // Map from googleDTO to UserDTO
            UserDTO userDTO = mapperGoogleToDtoUserDto(googleDTO);
            return authenticate(userDTO, request.getRememberMe());
        }
        return null;
    }

    @Override
    public AuthToken loginSSOByMicrosoftFlow2(MicrosoftLoginFlow2 request) {
        Optional<UserMicrosoftInfo> user = getUserInfoByAccessToken(request.getAccessToken());
        return null;
    }

    @Override
    public AuthToken loginSSOByMicrosoftFlow1(MicrosoftLoginFlow1 request) {
        Optional<String> accessToken = getAccessTokenByAuthorizationCode(request.getAuthorizationCode());
        if (accessToken.isPresent()) {
            Optional<UserMicrosoftInfo> user = getUserInfoByAccessToken(accessToken.get());
        }
        return null;
    }

    @Override
    public RedirectView getMicrosoftLogoutSsoUrl(String idToken, HttpServletResponse response) {
        String logoutUri = "https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/logout";
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("tenant_id", "xxxxx");
        String url = UriComponentsBuilder.fromUriString(logoutUri)
                .queryParam("id_token_hint", idToken)
                .queryParam(
                        "post_logout_redirect_uri",
                        URLEncoder.encode(
                                "http://localhost:3000/logout",
                                StandardCharsets.UTF_8))
                .uriVariables(uriVariables)
                .build()
                .toUriString();
        response.setHeader("location", url);
        response.setStatus(HttpStatus.OK.value());
        return new RedirectView(url);
    }

    @Override
    public RedirectView getMicrosoftLoginSsoUrl(HttpServletResponse response) {
        String templateUrl = "https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/authorize";
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("tenant_id", "xxxxx");
        String url = UriComponentsBuilder.fromUriString(templateUrl)
                .uriVariables(uriVariables)
                .queryParam(
                        "client_id", "xx")
                .queryParam(
                        "redirect_uri", URLEncoder.encode("http://localhost:3000",
                                StandardCharsets.UTF_8))
                //.queryParam("response_type", "code")
                .queryParam("response_type", URLEncoder.encode("code&id_token", StandardCharsets.UTF_8))
                .queryParam("response_mode", "query")
                .queryParam("scope", URLEncoder.encode("https://graph.microsoft.com/User.Read", StandardCharsets.UTF_8))
                .queryParam("state", "23211231342sdfs")
                .build()
                .toUriString();
        response.setHeader("location", url);
        response.setStatus(HttpStatus.OK.value());
        return new RedirectView(url);
    }

    Optional<UserMicrosoftInfo> getUserInfoByAccessToken(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(accessToken);
        String apiUserInfo = "https://graph.microsoft.com/v1.0/me";
        UserMicrosoftInfo userInfo = restTemplateHelper.executeWithPredefinedHeader(apiUserInfo, HttpMethod.GET, headers, UserMicrosoftInfo.class);
        if (Objects.nonNull(userInfo)) {
            log.info("User microsoft info: {}", userInfo);
            return Optional.of(userInfo);
        }
        return Optional.empty();
    }

    private Optional<String> getAccessTokenByAuthorizationCode(String authorizationCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        String azureClientId = "06a669c6-6e3e-4f01-a624-7f080e92d7e6";
        String azureClientSecret = "~0a7Q~fO80_unflYKHaf5nba8mmlm6BprSpvB";
        String azureTenantId = "06f1b89f-07e8-464f-b408-ec1b45703f31";

        MultiValueMap<String, String> formData = new LinkedMultiValueMap<String, String>();
        formData.add("client_id", azureClientId);
        formData.add("client_secret", azureClientSecret);
        formData.add("grant_type", "authorization_code");
        formData.add("scope", "https://graph.microsoft.com/User.Read");
        formData.add("code", authorizationCode);
        formData.add("redirect_uri", "http://localhost:3000"); //map authorizationCode from FE

        String azureUrlToken = String.format("https://login.microsoftonline.com/%s/oauth2/v2.0/token", azureTenantId);
        AuthToken authTokenSSO = restTemplateHelper.executeWithPredefinedHeader(azureUrlToken, HttpMethod.POST, headers, formData, AuthToken.class);
        if (Objects.nonNull(authTokenSSO)) {
            return Optional.of(authTokenSSO.getAccessToken());
        }
        return Optional.empty();
    }

    private AuthToken authenticate(UserDTO userDTO, Boolean isRememberMe) {
        User user = userService.authenticationSocialUser(userDTO);
        //Test
        user = new User();
        user.setId(1111L);
        user.setLogin(userDTO.getLogin());

        List<GrantedAuthority> authorities = new ArrayList<>();
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getLogin(), null, authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String accessToken = tokenProvider.createToken(authentication, user.getId());
        long expiresIn = authenticationProperties.getAccessTokenExpiresIn().toSeconds();
        String refreshToken = null;
        if (Boolean.TRUE.equals(isRememberMe)) {
            refreshToken = tokenProvider.createRefreshToken(user.getId());
        }
        return AuthToken.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .tokenType("Bearer")
                .expiresIn(expiresIn)
                .build();
    }

    private UserDTO mapperGoogleToDtoUserDto(GoogleDTO dto) {
        UserDTO userDTO = new UserDTO();
        if (!StrUtil.isBlank(dto.getName())) {
            userDTO.setName(dto.getName());
        }
        if (!StrUtil.isBlank(dto.getEmail())) {
            userDTO.setLogin(dto.getEmail().split("@")[0]);
            userDTO.setEmail(dto.getEmail());
        } else if (!StrUtil.isBlank(userDTO.getName())) {
            String login = StrUtil.removeAccent(dto.getName());
            login = login.replace(" ", "_").toLowerCase();
            userDTO.setLogin(login);
        } else {
            userDTO.setLogin(String.format("%s_%s", "google", dto.getSub()));
        }

        if (!StrUtil.isBlank(dto.getPicture())) {
            userDTO.setAvatar(dto.getPicture());
        }

        userDTO.setSocialId(dto.getSub());
        userDTO.setSocialType("google");
        return userDTO;
    }

}
