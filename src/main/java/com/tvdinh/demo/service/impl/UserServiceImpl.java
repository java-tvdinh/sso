package com.tvdinh.demo.service.impl;

import com.tvdinh.demo.domain.User;
import com.tvdinh.demo.domain.UserDTO;
import com.tvdinh.demo.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return Optional.empty();
    }

    @Override
    public User createUser(UserDTO userDTO) {
        return null;
    }

    @Override
    public User authenticationSocialUser(UserDTO userDTO) {
        // save user
        return null;
    }
}
