# SSO

#EX
- FE: https://gitlab.com/java-tvdinh/sso/-/tree/main/frontend
- 1 ứng dụng khác tên là B muốn sử dụng 1 api /xx bên A
    b1. Bên A sinh clientId, clientSecret lưu vào file yml
    b2. Bên B sẽ lấy token chứa thông tin(clientId, clientSecret) từ A bằng cách sử dụng token từ bên B call api /authen/b ở bên A
    b3. Bên A lấy được token bên B sẽ parser để lấy thông tin từ token(chứa user info) của B rồi map với user bên A
    b3. Sau khi map được user bên A -> sinh token chứa (clientId, clientSecret) để bên B call api
    b4. Bên B call api /xx gắn token chứa (clientId, clientSecret)
    b5. Bên A sẽ thêm filter để set SecurityContextHolder.getContext().setAuthentication() từ token b4

#Mircosoft
1. Đăng ký application: https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade
   + Chọ account types
   + Redirect URI ==> chọn "web", "http://localhost"
![IMAGE_DESCRIPTION](Tài liệu/microsoft.png)
2. Sidebar "Authentication" ==> select: access tokens and id tokens
3. Sidebar "Certification & secrets" ==> new client secret
![IMAGE_DESCRIPTION](Tài liệu/backend-aad.jpg)

```
Cách impl theo flow1:
b1. FE click vào button login sso -> FE call api tới BE ví dụ /authenticate/sso/gen-login
b2. BE khi nhận được request sẽ build url và trả về RedirectView (Thay vì FE tự build url login của SSO thì BE sẽ build và trả về redirect tới trang SSO để login)
b3. Sau khi xác thực SSO thành công sẽ trả về code tới FE(cấu hình khi build url redirect tới FE ở b2), sau đó fe call api /authenticate/sso/login tới BE truyền về auth_code
b4. BE nhận được auth_code sẽ call SSO để nhận token, sau đó xác thực,.... và trả về FE token của hệ thống + id_token(dùng để logout)
b5. FE muốn logout sẽ truyền về id_token để BE call api logout SSO /sso/logout
==> Làm ntn thì FE sẽ ko cần tham số clientId, clientSecret,.... của SSO mà BE sẽ giữ toàn bộ cấu hình này
```

# Đăng nhập user thuộc tenant khác
Lỗi khi đăng nhập với user khác tenant
![IMAGE_DESCRIPTION](Tài liệu/microsoft_error_login.jpg)
Giải pháp thêm người vào application
![IMAGE_DESCRIPTION](Tài liệu/microsoft_add_user.png)

